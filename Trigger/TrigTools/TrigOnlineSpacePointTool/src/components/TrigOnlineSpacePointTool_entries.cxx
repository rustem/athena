
#include "TrigOnlineSpacePointTool/PixelSpacePointTool.h"
#include "TrigOnlineSpacePointTool/SCT_SpacePointTool.h"
#include "TrigOnlineSpacePointTool/PixelClusterCacheTool.h"
#include "TrigOnlineSpacePointTool/SCT_ClusterCacheTool.h"
#include "TrigOnlineSpacePointTool/OnlineSpacePointProviderTool.h"
#include "TrigOnlineSpacePointTool/FastSCT_RodDecoder.h"
#include "../TrigL2LayerNumberTool.h"
#include "../TrigSpacePointConversionTool.h"

DECLARE_COMPONENT( PixelSpacePointTool )
DECLARE_COMPONENT( SCT_SpacePointTool )
DECLARE_COMPONENT( PixelClusterCacheTool )
DECLARE_COMPONENT( SCT_ClusterCacheTool )
DECLARE_COMPONENT( FastSCT_RodDecoder )
DECLARE_COMPONENT( OnlineSpacePointProviderTool )
DECLARE_COMPONENT( TrigL2LayerNumberTool )
DECLARE_COMPONENT( TrigSpacePointConversionTool )

